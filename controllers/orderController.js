//Dependencies and Modules
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");


//Non-admin User checkout (Create Order)
module.exports.checkout = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.user.email });
        if (!user) {
            return res.json({ status: false, message: 'User not found! Please login and try again.' });
        }

        const cart = await Cart.findOne({ userId: user.id });
        if (!cart) {
            return res.json({ status: true, message: "Your cart is empty. Shop now!" })
        } else {

            const currentDate = new Date();

            const newOrder = new Order({
                userId: user.id,
                products: cart.products,
                shippingAddress: req.body.shippingAddress,
                totalAmount: cart.totalAmount,
                orderDate: currentDate,
            });

            await newOrder.save(); //saves the order in the database
            await Cart.findByIdAndDelete(cart.id); //deletes the user's cart after checkout

            return res.json({
                status: true,
                message: "Order placed successfully! Your cart is now empty.",
                orderDetails: newOrder
            });
        }
    } catch (error) {
        return res.json({
            status: false,
            message: "Failed to place order. Try again later.",
            details: error.message
        });
    }
}

//STRETCH GOAL
//Retrieve authenticated user's orders
module.exports.getMyOrders = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.user.email });
        if (!user) {
            return res.json({ message: 'User not found! Please login and try again.' });
        }

        const orders = await Order.find({userId: req.params.userId});
        if (!orders.length) {
            return res.json(false);
        } else {


            let myOrders = [];

            // Assuming 'data' is the array containing order objects
            await Promise.all(orders.map(async (order) => {
                const products = await Promise.all(order.products.map(async (product) => {
                    const item = await Product.findById(product.productId);
                    return {
                        productId: product.productId,
                        quantity: product.quantity,
                        _id: product._id,
                        itemId: item._id,
                        name: item.name,
                        image: item.image,
                        description: item.description,
                        price: item.price,
                    };
                }));

                myOrders.push({
                    _id: order._id,
                    userId: order.userId,
                    shippingAddress: order.shippingAddress,
                    products: products,
                    totalAmount: order.totalAmount,
                    orderDate: order.orderDate,
                });

                
            }));

            myOrders.sort((a, b) => b.orderDate - a.orderDate);

            return res.json(myOrders);
        }
    } catch (error) {
        return res.json({
            message: "Something's wrong. Try again later.",
            details: error.message
        });
    }
}

//STRETCH GOAL
//Retrieve all orders (Admin only)
module.exports.getAllOrders = async (req, res) => {
    try {
        const orders = await Order.find({});
        res.json(orders);
    } catch (error) {
        res.json({
            message: "Something's wrong. Try again later.",
            details: error.message
        });
    }
}

module.exports.recentOrder = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.user.email });
        if (!user) {
            return res.json({ message: 'User not found! Please login and try again.' });
        }

        const result = await Order.find()
            .sort({ orderDate: -1 }) // Sort by the "createdAt" field in descending order (most recent first)
            .limit(1) // Limit the result to 1 document (the most recent one)

        if (result.length > 0) {

            return res.json(
                result[0]
            );
        } else {
            // No data found
            return res.json({
                message: 'No recent data found.'
            });
        }

    } catch (error) {
        return res.json({
            message: "Sorry, an error occured. Try again later.",
            error: error.message
        });
    }
}
