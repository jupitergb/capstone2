//Dependencies and Modules
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");

let logs = []; //Placeholder of the system logs for tracking of actions done

//STRETCH GOAL
//View Cart
module.exports.viewCart = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.user.email });
        if (!user) {
            return res.json({ message: 'User not found! Please login and try again.' });
        }

        const cart = await Cart.findOne({ userId: user.id });
        //If cart is empty or not yet created, it would prompt a corresponding message.
        if (!cart) {
            return res.json({ products: [], message: "Your cart is lonely. Shop now!" });
        } else {
            //Creates a more detailed cart information including the Product Name and Description.
            const products = [];
            for (const item of cart.products) {
                const product = await Product.findById(item.productId);
                products.push({
                    id: item.id,
                    productId: item.productId,
                    name: product.name,
                    description: product.description,
                    image: product.image,
                    price: product.price,
                    quantity: item.quantity,
                    subTotal: item.subTotal,
                })
            }
            const log = logs;
            logs = []; //cleared the logs

            return res.json({
                message: "Here's what's in your cart. Checkout now!",
                products,
                totalAmount: cart.totalAmount,
                systemLogs: log
            });
        }
    } catch (error) {
        res.json({
            message: "I'm sorry, we encountered a problem. Try again later.",
            error: error.message
        });
    }
};


//Checks if there is a duplicate product ID in the request.body
function hasDuplicateProduct(array) {
    const encounteredIds = new Set();
    for (const item of array) {
        if (encounteredIds.has(item.productId)) {
            return true;
        }
        encounteredIds.add(item.productId);
    }
    return false;
}


//Add a Product Only to Cart
module.exports.addProducts = async (req, res) => {
    try {
        const quantity = Number(req.params.quantity);
        const productId = req.params.productId;

        const user = await User.findOne({ email: req.user.email });
        const product = await Product.findById(productId);

        if (!user) {
            return res.json({
                status: false,
                message: 'User not found! Please login and try again.'
            });
        } else if (!product) {
            return res.json({
                status: false,
                message: 'Product not found! Please check if the Product ID is correct and try again.'
            });
        }

        const cart = await Cart.findOne({ userId: user.id });
        if (!cart) {
            newCart = {
                userId: user.id,
                products: [
                    {
                        productId: productId,
                        quantity: quantity,
                        subTotal: product.price * quantity
                    }
                ],
                totalAmount: product.price * req.params.quantity
            }
            await Cart.create(newCart);
            return res.json({
                status: true,
                newCart: newCart
            }
            );
        } else {
            const existingProduct = cart.products.find(cp => cp.productId.toString() === productId);
            if (existingProduct) {
                cart.totalAmount -= existingProduct.quantity * product.price;
                existingProduct.quantity += quantity;
                existingProduct.subTotal = existingProduct.quantity * product.price;
                cart.totalAmount += existingProduct.subTotal;
                await cart.save();
                return res.json({
                    status: true,
                    message: "Updated the product's quantity.",
                    cart
                })
            } else {
                cart.products.push({
                    productId: productId,
                    quantity: quantity,
                    subTotal: product.price * quantity
                });
                cart.totalAmount += product.price * quantity;
                await cart.save();
                return res.json({
                    status: true,
                    message: "Added the product to your cart.",
                    cart
                })
            }
        }
    } catch (error) {
        return res.json({
            status: false,
            message: "Sorry, an error occured. Try again later.",
            error: error.message
        })
    }
}


//Change Quantity Only of a Product in Cart
module.exports.changeQuantity = async (req, res) => {
    try {
        const quantity = Number(req.params.quantity);
        const productId = req.params.productId;

        const user = await User.findOne({ email: req.user.email });
        const product = await Product.findById(productId);

        if (!user) {
            return res.json({ status: false, message: 'User not found! Please login and try again.' });
        } else if (!product) {
            return res.json({ status: false, message: 'Product not found! Please check if the Product ID is correct and try again.' });
        }

        const cart = await Cart.findOne({ userId: user.id });

        if (!cart) {
            return res.json({
                status: false,
                message: "Product is not yet in your cart. Shop nowww!"
            });
        }

        const existingProduct = cart.products.find(cp => cp.productId.toString() === productId);

        if (existingProduct && quantity > 0) {
            cart.totalAmount -= existingProduct.quantity * product.price;
            existingProduct.quantity = quantity;
            existingProduct.subTotal = existingProduct.quantity * product.price;
            cart.totalAmount += existingProduct.subTotal;
            await cart.save();
            return res.json({
                status: true,
                message: "Updated the product's quantity.",
                cart
            });
        } else if (existingProduct && quantity <= 0) {
            cart.totalAmount -= existingProduct.subTotal;
            let index = cart.products.indexOf(existingProduct);
            cart.products.splice(index, 1);
            await cart.save();

            if (!cart.products.length) {
                await Cart.findByIdAndDelete(cart.id);
                return res.json({
                    status: true,
                    message: "You've emptied your cart. Shop now!",
                });
            } else {
                return res.json({
                    status: true,
                    message: "Product removed from your cart.",
                });
            }
        } else {
            return res.json({
                status: false,
                message: "Product is not yet in your cart. Shop now!"
            });
        }
    } catch (error) {
        return res.json({
            status: false,
            message: "Sorry, an error occured. Try again later.",
            error: error.message
        })
    }
}

//Remove a Product Only in Cart
module.exports.removeProduct = async (req, res) => {
    try {
        const productId = req.params.productId;

        const user = await User.findOne({ email: req.user.email });
        const product = await Product.findById(productId);

        if (!user) {
            return res.json({ status: false, message: 'User not found! Please login and try again.' });
        } else if (!product) {
            return res.json({ status: false, message: 'Product not found! Please check if the Product ID is correct and try again.' });
        }

        const cart = await Cart.findOne({ userId: user.id });

        if (!cart) {
            return res.json({
                status: true,
                message: "Your cart is empty. There is nothing to remove. Shop now!"
            });
        }

        const existingProduct = cart.products.find(cp => cp.productId.toString() === productId);
        if (existingProduct) {
            cart.totalAmount -= existingProduct.subTotal;
            let index = cart.products.indexOf(existingProduct);
            cart.products.splice(index, 1);
            await cart.save();

            if (!cart.products.length) {
                await Cart.findByIdAndDelete(cart.id);
                return res.json({
                    status: true,
                    message: "You've emptied your cart. Shop now!",
                });
            } else {
                return res.json({
                    status: true,
                    message: "Product removed from your cart.",
                });
            }
        } else {
            return res.json({
                status: true,
                message: "Product is not yet in your cart. Shop now!"
            });
        }
    } catch (error) {
        return res.json({
            status: false,
            message: "Sorry, an error occured. Try again later.",
            error: error.message
        })
    }
}

//Clear User's Cart
module.exports.clearCart = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.user.email });
        if (!user) {
            return res.json({ message: 'User not found! Please login and try again.' });
        }

        const deletedCart = await Cart.findOneAndDelete({ userId: user.id });
        if (!deletedCart) {
            return res.json({
                message: "Your cart is empty. There is nothing to clear. Shop now!"
            });
        } else {
            return res.json({
                message: "You have successfully cleared your cart. Shop now!"
            });
        }
    } catch (error) {
        return res.json({
            message: "Sorry, an error occured. Try again later.",
            error: error.message
        });
    }
}









//cpstn2-ecommerceapi-baliguat
//Manage Cart All at Once (Change Quantity to Add/Edit/Remove Products from Cart)
//If the given quantity >= 1 and the product is NOT active, it is skipped (no actions taken).
//If the given quantity >= 1 and product is active and it is not yet in cart, it will add the product in the cart.
//If the given quantity >= 1 and product is active and the product is already in the cart, it will update the product quantity to the given value.
//If the given quantity is 0 and product is NOT in the cart yet, it is skipped (no actions taken).
//If the given quantity is 0 and the product is in the cart, it will remove the product from the cart.
//If the cart ends up being empty, it will delete the user's cart.
module.exports.manageCart = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.user.email });
        if (!user) {
            return res.json({ message: 'User not found! Please login and try again.' });
        }

        const cart = await Cart.findOne({ userId: user.id });
        const orders = req.body;
        logs = [];

        if (hasDuplicateProduct(orders)) {
            return res.json({ message: "Ooops... Your input has a duplicate productId. Please modify and try again." })
        }

        if (!cart) {
            logs.push("No cart found. Creating a new cart...");
            console.log("\nNo cart found. Creating a new cart...");
            const newCart = {
                userId: user.id,
                products: [],
                totalAmount: 0
            };
            //Loop through each product in the request body
            for (const orderData of orders) {
                const { productId, quantity } = orderData;
                const product = await Product.findById(productId);
                //If the given quantity of a product is 0, it is skipped and no further actions taken.
                if (!quantity) {
                    logs.push(`(SKIPPED) ${quantity} x "${product.name}"`);
                    console.log(`(SKIPPED) ${quantity} x "${product.name}"`);
                    continue;
                    //If product is not active, it is not added to cart.
                } else if (quantity && !product.isActive) {
                    logs.push(`(UNAVAILABLE) ${quantity} x "${product.name}"`);
                    console.log(`(UNAVAILABLE) ${quantity} x "${product.name}"`);
                    continue;
                    //If given quantity is not 0 and product is active, it will add the product to the Cart.
                } else {
                    const subTotal = product.price * quantity;
                    newCart.products.push({ productId, quantity, subTotal });
                    newCart.totalAmount += subTotal;
                    logs.push(`(NEW) ${quantity} x "${product.name}", Subtotal: ${subTotal}`);
                    console.log(`(NEW) ${quantity} x "${product.name}", Subtotal: ${subTotal}`);
                }
            }

            logs.push(`Total Amount: ${newCart.totalAmount}`);
            console.log(`Total Amount: ${newCart.totalAmount}`);
            //If cart is not empty after iteration of the request body, it will create a new cart.
            if (newCart.products.length) {
                await Cart.create(newCart);
                logs.push("Cart successfully saved to the database.");
                console.log("Cart successfully saved to the database.");
                return this.viewCart(req, res);
                //If all given quantities are 0, it will not proceed in creating a new cart.
            } else {
                logs.push("Cart creation FAILED.");
                console.log("Cart creation FAILED.");
                const log = logs;
                logs = [];
                return res.json({
                    message: "All given quantities must not be 0. Please try again",
                    systemLogs: log
                })
            }
            //If cart already exists, it will update the products in the cart.        
        } else {
            logs.push("Updating your cart...");
            console.log("\nUpdating your cart...");
            //Loop through each product in the request body
            for (const orderData of orders) {
                const { productId, quantity } = orderData;
                const product = await Product.findById(productId);
                if (!product) {
                    return res.json({ message: "You may have entered an invalid Product ID. Please check and try again." })
                }

                //This finds the product from the request body in the existing array of products in the user's cart.
                const existingProduct = cart.products.find(cp => cp.productId.toString() === productId);

                //If existing product is found, it's quantity and subTotal are modified based on the given values.
                if (existingProduct) {
                    existingProduct.quantity = quantity;
                    existingProduct.subTotal = existingProduct.quantity * product.price;

                    let status = "(UPDATED)";

                    //If quantity ends up being 0, it is removed from the cart.
                    if (!existingProduct.quantity) {
                        const index = cart.products.indexOf(existingProduct);
                        cart.products.splice(index, 1);
                        status = "(REMOVED)";
                    }
                    logs.push(`${status} ${quantity} x "${product.name}", Subtotal: ${existingProduct.subTotal}`);
                    console.log(`${status} ${quantity} x "${product.name}", Subtotal: ${existingProduct.subTotal}`);
                    //If the product is not found in the user's cart, it will add it to the cart.
                } else {
                    //If the given quantity of a product is 0, it is skipped and no further actions taken.
                    if (!quantity) {
                        logs.push(`(SKIPPED) ${quantity} x "${product.name}"`);
                        console.log(`(SKIPPED) ${quantity} x "${product.name}"`);
                        continue;
                        //If product is not active, it is not added to cart.
                    } else if (quantity && !product.isActive) {
                        logs.push(`(UNAVAILABLE) ${quantity} x "${product.name}"`);
                        console.log(`(UNAVAILABLE) ${quantity} x "${product.name}"`);
                        continue;
                    } else {
                        //If product is not existing in the cart, adding the product.
                        const subTotal = product.price * quantity;
                        cart.products.push({ productId, quantity, subTotal });
                        logs.push(`(NEW) ${quantity} x "${product.name}" added to cart. Subtotal: ${subTotal}`)
                        console.log(`(NEW) ${quantity} x "${product.name}" added to cart. Subtotal: ${subTotal}`)
                    }
                }
                cart.totalAmount = cart.products.reduce((total, cp) => total + cp.subTotal, 0);
            }
            //If the cart ends up having empty products, this will delete the user's cart from the database.
            if (!cart.products.length) {
                await Cart.findOneAndDelete({ userId: user.id });
                logs.push("Cart is now empty.");
                console.log("Cart is now empty.")

                const log = logs;
                logs = [];

                return res.json({
                    message: "Oh no! You emptied your cart. Add products now!",
                    systemLogs: log
                })
            }

            logs.push(`Total Amount: ${cart.totalAmount}`);
            console.log(`Total Amount: ${cart.totalAmount}`);
            logs.push("Cart Updated.");
            console.log("Cart Updated!");

            await cart.save();
            //Return the View Cart functionality automatically for the user to see what's in the cart.
            return this.viewCart(req, res);
        }


    } catch (error) {
        //Returns an error if the request body is not an array of objects.
        if (error.message == "array is not iterable") {
            console.log("Transaction not successful. Error:", error.message)
            res.json({
                message: "Your given values must be an array of objects(products). Please try again."
            });
        } else {
            console.log("Transaction not successful. Error:", error.message)
            res.json({
                message: "I'm sorry, we encountered a problem.",
                error: error.message
            });
        }
    }
};