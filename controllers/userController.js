//Dependencies and Modules
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User Registration
module.exports.registerUser = async (req, res) => {
    try {
        const user = await User.findOne({email: req.body.email});

        //If user already exists, prompt to login or try a new email.
        if(user) {
            return res.json({message: "Email already registered! Please login or try a new email."})
        //If the user does not exist, it creates a new user in the database.
        } else {
            const newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 10)
            });    

            await newUser.save();
            return res.json({
                message: "New User Registered Successfully.",
                details: {
                    firstName: newUser.firstName,
                    lastName: newUser.lastName,
                    email: newUser.email,
                    password: "********",
                    isAdmin: newUser.isAdmin
                }
            });
        }
    } catch(error) {
        return res.json({
            message: "Internal Server Error. Try again.",
            details: error.message
        });
    }    
}

//User Authentication
module.exports.loginUser = async (req, res) => {
    try {
        const user = await User.findOne({email: req.body.email});
        //If user is not found, it prompts the user to register instead or try again.
        if(!user) {
            res.json({message: "Email not registered yet. Register now or try again."});
        //If user is found, it will attempt to login the user.
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password)
            if(isPasswordCorrect) {
                return res.json({
                    message: "Logged in successfuly!",
                    accessToken: auth.createAccessToken(user)
                })
            }
            else {
                return res.json({message: "Password is not correct. Try again."});
            }
        }
    } catch(error) {
        return res.json({
            message: "Ooops. An error occured.",
            details: error.message
        });
    }	
}

//Retrieve User Details
module.exports.userDetails = async (req, res) => {
    try {        
        const user = await User.findOne({email: req.user.email});        
        if(!user) {
            return res.json({message: "Hmmm... We can't find your profile information. Try again later."})
        } else {
            //Only returns the non-sensensitive profile information
            //ObjectId is omitted. Password is masked.
            req.user.password = "********";

            return res.json({
                
                
                    id: user._id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    password: req.user.password,
                    isAdmin: user.isAdmin
            });
        }        
    } catch(error) {
        return res.json({
            message: "Ooops. We encountered a problem. Try again later.",
            details: error.message
        });
    }
}

//Retrieve User Details using User ID (Admin Only)
module.exports.userDetailsforAdmin = async (req, res) => {
    try {        
        const user = await User.findById(req.params.userId);        
        if(!user) {
            return res.json({message: "Hmmm... We can't find your profile information. Try again later."})
        } else {
            //Only returns the non-sensensitive profile information
            //ObjectId is omitted. Password is masked.
            req.user.password = "********";

            return res.json({
                
                
                    id: user._id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    password: req.user.password,
                    isAdmin: user.isAdmin
            });
        }        
    } catch(error) {
        return res.json({
            message: "Ooops. We encountered a problem. Try again later.",
            details: error.message
        });
    }
}

//Retrieve User Details using User ID (Admin Only)
module.exports.allUsers = async (req, res) => {
    try {        
        const user = await User.find({}).select('-password');        
        if(!user) {
            return res.json({message: "Hmmm... We can't find any profile information. Try again later."})
        } else {
            

            return res.json(user);
        }        
    } catch(error) {
        return res.json({
            message: "Ooops. We encountered a problem. Try again later.",
            details: error.message
        });
    }
}

//STRETCH GOAL
//Set user as admin
module.exports.setAdmin = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(
            req.params.id, 
            {isAdmin: req.params.isAdmin},
            {new: true}
        );

        if(!user) {
            return res.json({ status: false, message: "User not found! Please check the User ID is correct and try again."});
        } else {
            return res.json({
                status: true,
                message: `User ${user.email} successfully changed Admin Status to ${user.isAdmin}`
            });
        }        
    } catch(error) {
        return res.json({
            status: false,
            message: "Something's wrong. Try again later.",
            details: error.message
        });
    }
}


//Change User Password
module.exports.changePassword = async (req, res) => {
    try {
        const user = await User.findOne({email: req.user.email});
        if(!user) {
            return res.json({
                message: "User not found! Please login and try again."
            });
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.oldPassword, user.password)
        
            if(isPasswordCorrect) {
                const newPassword = bcrypt.hashSync(req.body.newPassword, 10);
                user.password = newPassword;
                await user.save();
                return res.json({
                    message: "Password changed successfuly! Please login using the new password."
                });
            } else {
                return res.json({
                    message: "Incorrect Password. Try again."
                });
            }    
        }        
    } catch(error) {
        return res.json({
            message: "We encountered a problem. Try again later.",
            details: error.message
        });
    }   
}