//Dependencies and Modules
const Product = require("../models/Product");

//Create Product (Admin only)
module.exports.createProduct = async (req, res) => {
    try {
        let newProduct = new Product({
            name: req.body.name,
            image: req.body.image,
            description: req.body.description,
            price: req.body.price
        });

        await newProduct.save();
        return res.json({
            status: true,
            message: "New Product Registered Successfully!",
            newProduct
        });
    } catch (error) {
        return res.json({
            status: false,
            message: "Failed to register a new product. Try again later.",
            details: error.message
        });
    }
}

//Retrieve all products
module.exports.retrieveAllProducts = async (req, res) => {
    try {
        const allProducts = await Product.find({});
        return res.json(allProducts);
    } catch (error) {
        return res.json({
            message: "Failed to retrieve all products. Try again later.",
            details: error.message
        });
    }
}

//Retrieve all active products
module.exports.retrieveActiveProducts = async (req, res) => {
    try {
        const activeProducts = await Product.find({ isActive: true });
        return res.json(activeProducts);
    } catch (error) {
        return res.json({
            message: "Failed to retrieve the active products. Try again later.",
            details: error.message
        });
    }
}

//Retrieve single product
module.exports.retrieveSingleProduct = async (req, res) => {
    try {
        const product = await Product.findById(req.params.productId);
        if (!product) {
            return res.json({ message: "Product not found. Try a different Product ID." })
        } else {
            return res.json(product);
        }
    } catch (error) {
        res.json({
            message: "Failed to retrieve the product. Try again later.",
            details: error.message
        });
    }
}

//Update Product information (Admin only)
module.exports.updateProduct = async (req, res) => {
    try {
        let updatedProduct = {
            name: req.body.name,
            image: req.body.image,
            description: req.body.description,
            price: req.body.price
        };

        updatedProduct = await Product.findByIdAndUpdate(req.params.productId, updatedProduct, { new: true });

        if (!updatedProduct) {
            return res.json({
                status: false,
                message: "Update not successful! Product not found. Please check if the Product ID is correct and try again."
            });
        } else {
            return res.json({
                status: true,
                message: "Product updated successfully!",
                updatedProduct
            }
            );
        }
    } catch (error) {
        return res.json({
            status: false,
            message: "Failed to update the product. Try again later.",
            details: error.message
        });
    }
}

//Archive Product (Admin only)
module.exports.archiveProduct = async (req, res) => {
    try {
        const archivedProduct = await Product.findByIdAndUpdate(req.params.productId, { isActive: false }, { new: true });
        if (!archivedProduct) {
            return res.json({
                status: false,
                message: "Archive not successful! Product not found. Please check if the Product ID is correct and try again."
            });
        } else {
            return res.json({
                status: true,
                message: "Archived the Product successfully!",
                archivedProduct
            });
        }
    } catch (error) {
        return res.json({
            status: false,
            message: "Failed to archive the product. Try again later.",
            details: error.message
        });
    }
}

//Activate Product (Admin only)
module.exports.activateProduct = async (req, res) => {
    try {
        const activatedProduct = await Product.findByIdAndUpdate(req.params.productId, { isActive: true }, { new: true });
        if (!activatedProduct) {
            return res.json({
                status: false,
                message: "Activation not successful! Product not found. Please check if the Product ID is correct and try again."
            });
        } else {
            return res.json({
                status: true,
                message: "Activated the Product successfully!",
                activatedProduct
            });
        }
    } catch (error) {
        res.json({
            status: false,
            message: "Failed to activate the product. Try again later.",
            details: error.message
        });
    }
}