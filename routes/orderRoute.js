//Dependencies and Modules
const express = require("express");
const orderController = require("../controllers/orderController");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Routing Component
const router = express.Router();

//Non-admin User checkout (Create Order)
router.post("/checkout", verify, orderController.checkout);

//STRETCH GOAL
//Retrieve authenticated user's orders
router.get("/myOrders/:userId", verify, orderController.getMyOrders);

//STRETCH GOAL
//Retrieve all orders (Admin only)
router.get("/all", verify, verifyAdmin, orderController.getAllOrders);

//Retrieve all orders (Admin only)
router.get("/recent", verify, orderController.recentOrder);



module.exports = router;