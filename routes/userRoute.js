//Dependencies and Modules
const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;


//Routing Component
const router = express.Router();

//User registration
router.post("/register", userController.registerUser);

//User authentication
router.post("/login", userController.loginUser);

//Retrieve User Details
router.get("/profile", verify, userController.userDetails);

//STRETCH GOAL
//Set user as admin
router.patch("/:id/:isAdmin", verify, verifyAdmin, userController.setAdmin);

//STRETCH GOAL
//Change User Password
router.patch("/changePassword", verify, userController.changePassword);

//Retrieve All Users
router.get("/profile/all", verify, verifyAdmin, userController.allUsers);

//Retrieve User Details using User ID
router.get("/profile/:userId", verify, verifyAdmin, userController.userDetailsforAdmin);



module.exports = router;