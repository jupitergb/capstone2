//Dependencies and Modules
const express = require("express");
const productController = require("../controllers/productController");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Routing Component
const router = express.Router();

//Create Product (Admin only)
router.post("/create", verify, verifyAdmin, productController.createProduct);

//Retrieve all products  (Admin only)
router.get("/retrieve/all", verify, verifyAdmin, productController.retrieveAllProducts);


//Retrieve all active products
router.get("/retrieve/active", productController.retrieveActiveProducts);

//Retrieve single product
router.get("/retrieve/:productId", productController.retrieveSingleProduct);

//Update Product information (Admin only)
router.put("/update/:productId", verify, verifyAdmin, productController.updateProduct);

//Archive Product (Admin only)
router.patch("/archive/:productId", verify, verifyAdmin, productController.archiveProduct);

//Activate Product (Admin only)
router.patch("/activate/:productId", verify, verifyAdmin, productController.activateProduct);



module.exports = router;