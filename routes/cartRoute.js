//Dependencies and Modules
const express = require("express");
const cartController = require("../controllers/cartController");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Routing Component
const router = express.Router();


//View Cart
router.get("/view", verify, cartController.viewCart);

//Manage Cart All at Once (Add/Edit/Remove)
router.post("/manage", verify, cartController.manageCart);

//Add a Product Only to Cart
router.post("/add/:productId/:quantity", verify, cartController.addProducts);

//Change Quantity Only of a Product in Cart
router.patch("/change/:productId/:quantity", verify, cartController.changeQuantity);

//Remove a Product Only in Cart
router.patch("/remove/:productId", verify, cartController.removeProduct);

//Clear User's Cart
router.delete("/clear", verify, cartController.clearCart);

module.exports = router;