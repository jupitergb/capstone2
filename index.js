//Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

//Environment Setup
const PORT = 4000;
const MONGO_URI = "mongodb+srv://baliguatjupiter:iamKira96@batch-297.vvkeu5v.mongodb.net/capstone2?retryWrites=true&w=majority";

//Server Setup
const app = express();

//Middlewares
app.use(express.json({limit: "30mb",extended:true}));
app.use(express.urlencoded({extended:true}));
app.use(cors());



//MongoDB Connection
mongoose.connect(
    MONGO_URI, 
    {
        useNewUrlParser: true,
	    useUnifiedTopology: true
    }
);

mongoose.connection.once('open',()=> console.log('Now Connected to MongoDB Atlas'));

app.get("/", async (req, res) => {
    res.sendFile(__dirname + '/readme.md');
})

//Backend Routes
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);
app.use("/carts", cartRoute);

//Server Gateway Response
app.listen(PORT, ()=> console.log(`Server listening on port: ${PORT}`));