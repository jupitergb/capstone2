const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User', // Reference to the User model
        required: [true, "User ID is required."]
    },
    products: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Product', // Reference to the Product model
                required : [true, "Product ID is required"]
            },
            quantity: {
                type: Number,
                required : [true, "Product Quantity is required"]
            }
        }
    ],
    shippingAddress: {
        type: String,
        required: [true, "Shipping Address is required."]
    },
    totalAmount: {
        type: Number,
        required: [true, "Order Total Amount is required."]
    },
    orderDate: {
        type: Date,
        default: new Date()
    }
});

//Model
module.exports = mongoose.model("Order", orderSchema);