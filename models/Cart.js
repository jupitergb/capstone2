const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User', // Reference to the User model
        unique: true,
        required: [true, "User ID is required."]
    },
    products: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Product', // Reference to the Product model
                required : [true, "Product ID is required"]
            },
            quantity: {
                type: Number,
                required : [true, "Product Quantity is required"]
            },
            subTotal: {
                type: Number,
                required : [true, "Subtotal is required"]
            }
        }
    ],
    totalAmount: {
        type: Number,
        required: [true, "Order Total Amount is required."]
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

//Model
module.exports = mongoose.model("Cart", cartSchema);