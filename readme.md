## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular Users:
     - email: john@mail.com
     - pwd: john1234

     - email: jane@mail.com
     - pwd: jane1234

- Admin User:
    - email: jupiter@mail.com
    - pwd: jupiter1234
    

***ROUTES:***
- User registration (POST)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/users/register
    - request body: 
        - email (string)
        - password (string)

- User authentication (POST)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/users/login
    - request body: 
        - email (string)
        - password (string)

- Create Product (Admin only) (POST)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/products/create
    - request body: 
        - name (string)
        - description (string)
        - price (number)

- Retrieve all products (Admin only) (GET)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/products/retrieve/all
    - request body: none

- Retrieve all active products (GET)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/products/retrieve/active
    - request body: none

- Retrieve single product (GET)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/products/retrieve/:productId
    - request body: none

- Update Product information (Admin only) (PUT)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/products/update/:productId
    - request body: 
        - name (string)
        - description (string)
        - price (number)

- Archive Product (Admin only) (PATCH)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/products/archive/:productId
    - request body: none

- Activate Product (Admin only) (PATCH)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/products/activate/:productId
    - request body: none

- Non-admin User checkout (Create Order) (Auth) (POST)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/orders/checkout
    - request body: none

- Retrieve User Details (Auth) (GET)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/users/profile
    - request body: none

- Set user as admin (Admin only) (PATCH)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/users/:id/:isAdmin
    - request body: none

- Retrieve authenticated user’s orders (Auth) (GET)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/orders/myOrders
    - request body: none

- Retrieve all orders (Admin only) (GET)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/orders/all
    - request body: none

- Change Password(Auth) (PATCH)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/users/changePassword
    - request body:
        - oldPassword (string)
        - newPassword (string)

- View Cart (Auth) (GET)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view
    - request body: none

- Add a Product Only to Cart (Auth) (POST)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/add/:productId/:quantity
    - request body: none

- Change Quantity Only of a Product in Cart (Auth) (PATCH)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/change/:productId/:quantity
    - request body: none

- Remove a Product Only in Cart (Auth) (PATCH)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/remove/:productId
    - request body: none

- Clear User's Cart (Auth) (DELETE)
	- https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/clear
    - request body: none

- Manage Cart All at Once (Add/Edit/Remove) (Auth) (POST)
	- http://localhost:4000/carts/manage
    - request body:
        - products (array)
            - productId (string)
            - quantity (number)
