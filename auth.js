const jwt = require("jsonwebtoken");
const secret = "nkzyA3Awx796nIkB2IlNSz6W8tp";

module.exports.createAccessToken = (user) => {
	const data = {
		email: user.email,
		password: user.password,
        isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
}

//Token verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token === "undefined") {
		return res.send({auth: "Failed. No Token"});
	} else {		
		token = token.slice(7, token.length);

		//Token decryption
		jwt.verify(token, secret, function(err, decodedToken) {
			if(err) {
				return res.send({auth: "Failed", message: err.message});
			} else {
				req.user = decodedToken;
				next();
			}
		})
	}
}

//Admin verification
module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin) {
		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		});
	}
}